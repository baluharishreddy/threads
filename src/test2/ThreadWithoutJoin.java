package test2;

public class ThreadWithoutJoin extends Thread
{
	public void run()
	{ 
		System.out.println("Before running....");
		try
		{
			Thread.sleep(1000);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		System.out.println("thread is running...");  
	}  

	public static void main(String[] args) 
	{
		ThreadWithoutJoin t1=new ThreadWithoutJoin();
		ThreadWithoutJoin t2=new ThreadWithoutJoin();
		t1.start();  
		try
		{
			t1.join();
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		t2.start();
	}

}
