package test2;

public class ThreadPriority extends Thread
{
	public void run()
	{  
		   System.out.println("Running");  		    		  
    }  
	public static void main(String[] args)
	{
		ThreadPriority t1=new ThreadPriority();  
		ThreadPriority t2=new ThreadPriority();  
		System.out.println("thread priority of t1 is:"+t1.getPriority());
		System.out.println("thread priority of t2 is:"+t2.getPriority());	
		t1.setPriority(2);  
		t2.setPriority(6); 
		System.out.println("now thread priority of t1 is:"+t1.getPriority());
		System.out.println("now thread priority of t2 is:"+t2.getPriority());
		System.out.println("running thread name is:"+Thread.currentThread().getName()); 
		System.out.println("running thread priority is:"+Thread.currentThread().getPriority()); 
		t1.start();  
		System.out.println("running thread name is:"+Thread.currentThread().getName()); 
		System.out.println("running thread priority is:"+Thread.currentThread().getPriority());  
		t2.start(); 
		System.out.println("running thread name is:"+Thread.currentThread().getName()); 
		System.out.println("running thread priority is:"+Thread.currentThread().getPriority());  
	}
}
