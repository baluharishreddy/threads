package test2;

public class IsAliveThread extends Thread    
{
	public void run()
	{ 
		System.out.println("Before running....");
		try
		{
			Thread.sleep(1000);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		System.out.println("thread is running...");  
	}  

	public static void main(String[] args) 
	{
		IsAliveThread t1=new IsAliveThread();
		IsAliveThread t2=new IsAliveThread();
		t1.start();  
		t2.start();
		System.out.println(t1.isAlive());
		System.out.println(t2.isAlive());
	}

}
