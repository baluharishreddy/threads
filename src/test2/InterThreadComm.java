package test2;

//What does notify do  there is no wait method?
//A: If there is no other thread waiting then not much will happen. It tries to notify a waiting thread if available,
//   otherwise it simply does nothing.


public class InterThreadComm
{
	boolean flag=false;
	synchronized void even()
	{
		if(!flag)
		{
			try
			{
				wait();
			}
			catch(Exception e)
			{
			}
		}
		for(int i=2;i<=10;i++)
		{
			if(!flag)
			{
				try
				{
					wait();
				}
				catch(Exception e)
				{
				}
			}
			if(i%2==0)
			{
				System.out.println(i+ "-even-"+(i*i));
			}
			flag=false;
			notify();
		}
	}
	synchronized void odd()
	{
		if(flag)
		{
			try
			{
				wait();
			}
			catch(Exception e)
			{
			}
		}
		for(int i=1;i<=10;i++)
		{
			if(flag)
			{
				try
				{
					wait();
				}
				catch(Exception e)
				{
				}
			}
			if(i%2!=0)
			{
				System.out.println(i+ "-odd-"+(i*i*i));
			}
			flag=true;
			notify();
		}
		
	}	

}
