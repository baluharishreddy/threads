package test2;

public class Account
{
	int balance;
	public Account()
	{
		balance=5000;
	}
	public synchronized void withdraw(int bal)
	{
		try
		{
			Thread.sleep(1000);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		balance=balance-bal;
		System.out.println("Balance remaining="+balance+" "+Thread.currentThread().getName());
	}

}
