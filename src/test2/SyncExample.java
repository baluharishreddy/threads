package test2;
class SubClassAccount implements Runnable
{
	Account obj;
	public SubClassAccount(Account a)
	{
		obj=a;
	}
	public void run()
	{ 
		obj.withdraw(500); 
	}  
}

public class SyncExample
{

	public static void main(String[] args)
	{
		Account a1=new Account();
		SubClassAccount c1=new SubClassAccount(a1);
		Thread t1=new Thread(c1,"a");
		Thread t2=new Thread(c1,"b");
		Thread t3=new Thread(c1,"c");
		t1.start();  
		t2.start();  
		t3.start();
	}

}
